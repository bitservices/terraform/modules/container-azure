###############################################################################
# Modules
###############################################################################

module "acr_registry" {
  source   = "../acr/registry"
  class    = "acr"
  group    = local.group
  owner    = local.owner
  company  = local.company
  location = local.location
}

###############################################################################
