###############################################################################
# Data Sources
###############################################################################

data "azurerm_subnet" "aks_cluster_node_pool" {
  name                 = format("%s-%s", local.vnet, local.aks_class)
  virtual_network_name = local.vnet
  resource_group_name  = local.group
}

###############################################################################
# Resources
###############################################################################

resource "random_password" "aks_cluster_password" {
  length = 16
}

###############################################################################

resource "tls_private_key" "aks_cluster_ssh" {
  algorithm   = "ECDSA"
  ecdsa_curve = "P384"
}

###############################################################################
# Modules
###############################################################################

module "aks_cluster" {
  source              = "../aks/cluster"
  key                 = tls_private_key.aks_cluster_ssh.public_key_openssh
  vnet                = local.vnet
  class               = local.aks_class
  group               = local.group
  owner               = local.owner
  company             = local.company
  location            = local.location
  password            = random_password.aks_cluster_password.result
  node_pool_subnet_id = data.azurerm_subnet.aks_cluster_node_pool.id
}

###############################################################################
