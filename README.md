<!---------------------------------------------------------------------------->

# container (azure)

<!---------------------------------------------------------------------------->

## Description

Manage containerisation services on [Azure] such as [Azure] Container
Registry ([ACR]) and [Azure] [Kubernetes] Service ([AKS]).

<!---------------------------------------------------------------------------->

## Modules

* [acr/registry](acr/registry/README.md) - Manage [Azure] Container Registry ([ACR]) registries.
* [aks/cluster](aks/cluster/README.md) - Manage [Azure] [Kubernetes] Service ([AKS]) clusters.

<!---------------------------------------------------------------------------->

[ACR]:        https://azure.microsoft.com/services/container-registry/
[AKS]:        https://azure.microsoft.com/services/kubernetes-service/
[Azure]:      https://azure.microsoft.com/
[Kubernetes]: https://kubernetes.io/

<!---------------------------------------------------------------------------->
