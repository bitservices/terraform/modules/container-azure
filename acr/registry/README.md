<!---------------------------------------------------------------------------->

# acr/registry

#### Manage [Azure] Container Registry ([ACR]) registries

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/container/azure//acr/registry`**

-------------------------------------------------------------------------------

### Example Usage

```
variable "owner"    { default = "terraform@bitservices.io" }
variable "company"  { default = "BITServices Ltd"          }
variable "location" { default = "uksouth"                  }

module "my_resource_group" {
  source   = "gitlab.com/bitservices/group/azure//resource"
  name     = "foobar"
  owner    = var.owner
  company  = var.company
  location = var.location
}

module "my_acr_registry" {
  source   = "gitlab.com/bitservices/container/azure//acr/registry"
  class    = "foobar"
  group    = module.my_resource_group.name
  owner    = var.owner
  company  = var.company
  location = var.location
}
```

<!---------------------------------------------------------------------------->

[ACR]:   https://azure.microsoft.com/services/container-registry/
[Azure]: https://azure.microsoft.com/

<!---------------------------------------------------------------------------->
