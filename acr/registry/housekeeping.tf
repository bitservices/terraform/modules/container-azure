###############################################################################
# Optional Variables
###############################################################################

variable "housekeeping_name" {
  type        = string
  default     = "acr-clean"
  description = "The full name of the housekeeping task within this container registry."
}

variable "housekeeping_enabled" {
  type        = bool
  default     = false
  description = "Should the housekeeping task be enabled for this container registry."
}

variable "housekeeping_timeout" {
  type        = number
  default     = 3600
  description = "The timeout of the housekeeping task in seconds. Must be between '300' and '28800'."
}

###############################################################################

variable "housekeeping_filter_keep" {
  type        = number
  default     = 10
  description = "How many tags should be kept for each repository as a minimum."
}

variable "housekeeping_filter_tags" {
  type        = string
  default     = ".*"
  description = "What tags (for every repository) should be housekept? Specified as a regular expression."
}

variable "housekeeping_filter_repos" {
  type        = string
  default     = ".*"
  description = "What repositories should be housekept? Specified as a regular expression."
}

variable "housekeeping_filter_age_days" {
  type        = number
  default     = 365
  description = "How old tags should be before being removed (unless excluded by 'housekeeping_filter_tags' or 'housekeeping_filter_keep') in days."
}

###############################################################################

variable "housekeeping_platform_os" {
  type        = string
  default     = "Linux"
  description = "The operating system type required for the housekeeping task. Must be 'Windows' or 'Linux'."
}

variable "housekeeping_platform_architecture" {
  type        = string
  default     = "amd64"
  description = "The OS architecture required for the housekeeping task. Possible values are 'amd64', 'x86', '386', 'arm' or 'arm64'."
}

###############################################################################

variable "housekeeping_timer_class" {
  type        = string
  default     = "timer"
  description = "Identifier for the timer trigger for the housekeeping task. The housekeeping task name is prepended to this automatically."
}

variable "housekeeping_timer_weekly" {
  type        = bool
  default     = true
  description = "Should the housekeeping task be ran weekly? If 'false' the task is ran daily. Ignored if 'housekeeping_timer_schedule' is set."
}

variable "housekeeping_timer_enabled" {
  type        = bool
  default     = true
  description = "Should the housekeeping task timer trigger be enabled?"
}

variable "housekeeping_timer_schedule" {
  type        = string
  default     = null
  description = "Override the timer trigger schedule for the housekeeping task. Must be in cron format. If not specified the task is ran at a random point daily or weekly depending on 'housekeeping_timer_weekly'."
}

###############################################################################
# Locals
###############################################################################

locals {
  housekeeping_timer_name     = format("%s-%s", var.housekeeping_name, var.housekeeping_timer_class)
  housekeeping_timer_random   = format("%s %s * * %s", random_integer.housekeeping_timer_minute.result, random_integer.housekeeping_timer_hour.result, var.housekeeping_timer_weekly ? random_integer.housekeeping_timer_day.result : "*")
  housekeeping_timer_weekly   = var.housekeeping_timer_schedule == null ? var.housekeeping_timer_weekly : null
  housekeeping_timer_schedule = coalesce(var.housekeeping_timer_schedule, local.housekeeping_timer_random)
}

###############################################################################
# Resources
###############################################################################

resource "random_integer" "housekeeping_timer_minute" {
  min = 0
  max = 59

  keepers = {
    "name"        = var.housekeeping_name
    "timer_class" = var.housekeeping_timer_class
  }
}

###############################################################################

resource "random_integer" "housekeeping_timer_hour" {
  min = 0
  max = 23

  keepers = {
    "name"        = var.housekeeping_name
    "timer_class" = var.housekeeping_timer_class
  }
}

###############################################################################

resource "random_integer" "housekeeping_timer_day" {
  min = 0
  max = 6

  keepers = {
    "name"        = var.housekeeping_name
    "timer_class" = var.housekeeping_timer_class
  }
}

###############################################################################

resource "azurerm_container_registry_task" "housekeeping" {
  name                  = var.housekeeping_name
  enabled               = var.housekeeping_enabled
  timeout_in_seconds    = var.housekeeping_timeout
  container_registry_id = azurerm_container_registry.object.id

  tags = {
    "Name"         = var.housekeeping_name
    "Group"        = var.group
    "Owner"        = var.owner
    "Company"      = var.company
    "Enabled"      = var.housekeeping_enabled ? "Yes" : "No"
    "Timeout"      = var.housekeeping_timeout
    "Location"     = var.location
    "Registry"     = local.name
    "Subscription" = data.azurerm_subscription.current.display_name
  }

  encoded_step {
    task_content = base64encode(templatefile(format("%s/housekeeping.yaml.tpl", local.templates), {
      "days"    = var.housekeeping_filter_age_days
      "keep"    = var.housekeeping_filter_keep
      "tags"    = var.housekeeping_filter_tags
      "repos"   = var.housekeeping_filter_repos
      "timeout" = var.housekeeping_timeout
    }))
  }

  platform {
    os           = var.housekeeping_platform_os
    architecture = var.housekeeping_platform_architecture
  }

  timer_trigger {
    name     = local.housekeeping_timer_name
    enabled  = var.housekeeping_timer_enabled
    schedule = local.housekeeping_timer_schedule
  }
}

###############################################################################
# Outputs
###############################################################################

output "housekeeping_filter_keep" {
  value = var.housekeeping_filter_keep
}

output "housekeeping_filter_tags" {
  value = var.housekeeping_filter_tags
}

output "housekeeping_filter_repos" {
  value = var.housekeeping_filter_repos
}

output "housekeeping_filter_age_days" {
  value = var.housekeeping_filter_age_days
}

###############################################################################

output "housekeeping_platform_os" {
  value = var.housekeeping_platform_os
}

output "housekeeping_platform_architecture" {
  value = var.housekeeping_platform_architecture
}

###############################################################################

output "housekeeping_timer_name" {
  value = local.housekeeping_timer_name
}

output "housekeeping_timer_class" {
  value = var.housekeeping_timer_class
}

output "housekeeping_timer_weekly" {
  value = local.housekeeping_timer_weekly
}

output "housekeeping_timer_enabled" {
  value = var.housekeeping_timer_enabled
}

output "housekeeping_timer_schedule" {
  value = local.housekeeping_timer_schedule
}

###############################################################################

output "housekeeping_id" {
  value = azurerm_container_registry_task.housekeeping.id
}

output "housekeeping_name" {
  value = azurerm_container_registry_task.housekeeping.name
}

output "housekeeping_enabled" {
  value = azurerm_container_registry_task.housekeeping.enabled
}

output "housekeeping_timeout" {
  value = azurerm_container_registry_task.housekeeping.timeout_in_seconds
}

###############################################################################
