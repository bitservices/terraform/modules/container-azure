###############################################################################
# Required Variables
###############################################################################

variable "class" {
  type        = string
  description = "The container registry identifier. This is suffixed to the resource group name and must be globally unique."
}

variable "group" {
  type        = string
  description = "The full name of the resource group that will contain this container registry."
}

variable "owner" {
  type        = string
  description = "Owner of the resource."
}

variable "company" {
  type        = string
  description = "Company the resource belogs to."
}

variable "location" {
  type        = string
  description = "Datacentre location for this container registry."
}

###############################################################################
# Optional Variables
###############################################################################

variable "sku" {
  type        = string
  default     = "Basic"
  description = "The SKU name of the container registry. Possible values are Basic, Standard and Premium."
}

variable "admin_enabled" {
  type        = bool
  default     = false
  description = "Specifies whether the admin user is enabled."
}

variable "replication_locations" {
  type        = list(string)
  default     = []
  description = "A list of Azure locations where the container registry should be geo-replicated. Premium sku only."
}

###############################################################################

variable "type_primary" {
  type        = string
  default     = "Primary"
  description = "The type tag to set for the primary container registry instance."
}

variable "type_replica" {
  type        = string
  default     = "Replica"
  description = "The type tag to set for any geo-replicas of this container registry."
}

###############################################################################
# Locals
###############################################################################

locals {
  name                  = format("%s%s", var.group, var.class)
  templates             = format("%s/templates", path.module)
  replication_locations = lower(var.sku) == "premium" ? var.replication_locations : []
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_container_registry" "object" {
  sku                 = var.sku
  name                = local.name
  location            = var.location
  admin_enabled       = var.admin_enabled
  resource_group_name = var.group

  dynamic "georeplications" {
    for_each = local.replication_locations

    content {
      location = georeplications.value

      tags = {
        "SKU"          = var.sku
        "Name"         = local.name
        "Type"         = var.type_replica
        "Class"        = var.class
        "Group"        = var.group
        "Owner"        = var.owner
        "Company"      = var.company
        "Location"     = georeplications.value
        "Subscription" = data.azurerm_subscription.current.display_name
      }
    }
  }

  tags = {
    "SKU"          = var.sku
    "Name"         = local.name
    "Type"         = var.type_primary
    "Class"        = var.class
    "Group"        = var.group
    "Owner"        = var.owner
    "Company"      = var.company
    "Location"     = var.location
    "Subscription" = data.azurerm_subscription.current.display_name
  }
}

###############################################################################
# Outputs
###############################################################################

output "class" {
  value = var.class
}

output "group" {
  value = var.group
}

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

output "location" {
  value = var.location
}

###############################################################################

output "sku" {
  value = var.sku
}

output "admin_enabled" {
  value = var.admin_enabled
}

output "replication_locations" {
  value = local.replication_locations
}

###############################################################################

output "type_primary" {
  value = var.type_primary
}

output "type_replica" {
  value = var.type_replica
}

###############################################################################

output "templates" {
  value = local.templates
}

###############################################################################

output "id" {
  value = azurerm_container_registry.object.id
}

output "uri" {
  value = azurerm_container_registry.object.login_server
}

output "name" {
  value = azurerm_container_registry.object.name
}

output "admin_username" {
  value = azurerm_container_registry.object.admin_username
}

output "admin_password" {
  value     = azurerm_container_registry.object.admin_password
  sensitive = true
}

###############################################################################
