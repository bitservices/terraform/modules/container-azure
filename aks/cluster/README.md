<!---------------------------------------------------------------------------->

# aks/cluster

#### Manage [Azure] [Kubernetes] Service ([AKS]) clusters

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/container/azure//aks/cluster`**

-------------------------------------------------------------------------------

### Example Usage

```
variable "owner"     { default = "terraform@bitservices.io" }
variable "company"   { default = "BITServices Ltd"          }
variable "location"  { default = "uksouth"                  }
variable "ipv4_cidr" { default = "10.100.0.0/16"            }
variable "ipv6_cidr" { default = "fd37:b175:5b87:9d00::/56" }

locals {
  my_aks_subnet_ipv4_cidr = cidrsubnet(var.ipv6_cidr, 1, 0)
  my_aks_subnet_ipv6_cidr = cidrsubnet(var.ipv4_cidr, 8, 0)
}

module "my_resource_group" {
  source   = "gitlab.com/bitservices/group/azure//resource"
  name     = "foobar"
  owner    = var.owner
  company  = var.company
  location = var.location
}

module "my_vnet" {
  source              = "gitlab.com/bitservices/network/azure//vnet"
  name                = "foobar1"
  cidrs               = tolist([ var.ipv6_cidr, var.ipv4_cidr ])
  group               = module.my_resource_group.name
  owner               = var.owner
  company             = var.company
  location            = var.location
  nat_gateway_enabled = true
}

module "my_aks_subnet" {
  source                              = "gitlab.com/bitservices/network/azure//subnet"
  vnet                                = module.my_vnet.name
  cidrs                               = tolist([ local.my_aks_subnet_ipv6_cidr, local.my_aks_subnet_ipv4_cidr ])
  class                               = "aks"
  group                               = module.my_resource_group.name
  owner                               = var.owner
  client                              = var.client
  location                            = var.location
  nat_gateway_id                      = module.my_vnet.nat_gateway_id
  nat_gateway_enabled                 = module.my_vnet.nat_gateway_enabled
  security_group_enable_vnet_out      = true
  security_group_enable_internet_out  = true
  security_group_enable_load_balancer = true

  security_group_rules_extra = [
    {
      "name"                   = "AllowHTTPIngressInBound"
      "access"                 = "Allow"
      "protocol"               = "TCP"
      "priority"               = 400
      "direction"              = "Inbound"
      "source_cidr"            = "*"
      "destination_cidr"       = "VirtualNetwork"
      "source_port_range"      = "*"
      "destination_port_range" = "80"
    },
    {
      "name"                   = "AllowHTTPSIngressInBound"
      "access"                 = "Allow"
      "protocol"               = "TCP"
      "priority"               = 401
      "direction"              = "Inbound"
      "source_cidr"            = "*"
      "destination_cidr"       = "VirtualNetwork"
      "source_port_range"      = "*"
      "destination_port_range" = "443"
    },
    {
      "name"                   = "AllowAKSSubnetIPv6"
      "access"                 = "Allow"
      "protocol"               = "*"
      "priority"               = 800
      "direction"              = "Inbound"
      "source_cidr"            = local.my_aks_subnet_ipv6_cidr
      "destination_cidr"       = local.my_aks_subnet_ipv6_cidr
      "source_port_range"      = "*"
      "destination_port_range" = "*"
    },
    {
      "name"                   = "AllowAKSSubnetIPv4"
      "access"                 = "Allow"
      "protocol"               = "*"
      "priority"               = 801
      "direction"              = "Inbound"
      "source_cidr"            = local.my_aks_subnet_ipv4_cidr
      "destination_cidr"       = local.my_aks_subnet_ipv4_cidr
      "source_port_range"      = "*"
      "destination_port_range" = "*"
    }
  ]
}

resource "tls_private_key" "my_aks_cluster_ssh_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "random_password" "my_aks_cluster_password" {
  length  = 32
  special = false

  keepers = {
    "key"   = tls_private_key.my_aks_cluster_ssh_key.public_key_openssh
    "vnet"  = module.my_vnet.name
    "group" = module.my_resource_group.name
  }
}

module "my_aks_cluster" {
  source              = "gitlab.com/bitservices/container/azure//aks/cluster"
  key                 = tls_private_key.my_aks_cluster_ssh_key.public_key_openssh
  vnet                = module.my_vnet.name
  class               = "aks"
  group               = module.my_resource_group.name
  owner               = var.owner
  company             = var.company
  location            = var.location
  password            = random_password.my_aks_cluster_password.result
  node_pool_subnet_id = module.my_aks_subnet.id
}
```

<!---------------------------------------------------------------------------->

[AKS]:        https://azure.microsoft.com/services/kubernetes-service/
[Azure]:      https://azure.microsoft.com/
[Kubernetes]: https://kubernetes.io/

<!---------------------------------------------------------------------------->
