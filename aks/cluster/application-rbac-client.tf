###############################################################################
# Optional Variables
###############################################################################

variable "application_rbac_client_class" {
  type        = string
  default     = "rbac-client"
  description = "The identifier to add to this AKS clusters name to identify the RBAC client application registration."
}

variable "application_rbac_client_image" {
  type        = string
  default     = null
  description = "Path to a PNG image to use as the applications logo."
}

variable "application_rbac_client_owners" {
  type        = list(string)
  default     = null
  description = "A list of object IDs that will be set as owners for this application. If unspecified the current context is used."
}

variable "application_rbac_client_audience" {
  type        = string
  default     = "AzureADMyOrg"
  description = "The Microsoft account types that are supported for the current application. Must be one of 'AzureADMyOrg', 'AzureADMultipleOrgs', 'AzureADandPersonalMicrosoftAccount' or 'PersonalMicrosoftAccount'."
}

variable "application_rbac_client_terms_url" {
  type        = string
  default     = null
  description = "URL of the application's terms of service statement."
}

variable "application_rbac_client_reply_urls" {
  type        = list(string)
  default     = null
  description = "A list of URLs that user tokens are sent to for sign in, or the redirect URLs that OAuth 2.0 authorization codes and access tokens are sent to."
}

variable "application_rbac_client_group_claims" {
  type        = set(string)
  default     = ["SecurityGroup"]
  description = "Configures the groups claim issued in a user or OAuth 2.0 access token that the app expects. Possible values are 'None', 'SecurityGroup', 'DirectoryRole', 'ApplicationGroup' or 'All'."
}

variable "application_rbac_client_identifier_urls" {
  type        = list(string)
  default     = null
  description = "A list of user-defined URLs that uniquely identify an application within it's Azure AD tenant, or within a verified custom domain if the application is multi-tenant."
}

variable "application_rbac_client_prevent_duplicates" {
  type        = bool
  default     = true
  description = "If 'true', will return an error if an existing application is found with the same name."
}

variable "application_rbac_client_privacy_statement_url" {
  type        = string
  default     = null
  description = "URL of the application's privacy statement."
}

###############################################################################

variable "application_rbac_client_feature_hide" {
  type        = bool
  default     = false
  description = "Whether this app is invisible to users in My Apps and Office 365 Launcher."
}

variable "application_rbac_client_feature_gallery" {
  type        = bool
  default     = false
  description = "Whether this application represents a gallery application for linked service principals."
}

variable "application_rbac_client_feature_custom_sso" {
  type        = bool
  default     = false
  description = "Whether this application represents a custom SAML application for linked service principals."
}

variable "application_rbac_client_feature_enterprise" {
  type        = bool
  default     = false
  description = "Whether this application represents an Enterprise Application for linked service principals."
}

###############################################################################
# Locals
###############################################################################

locals {
  application_rbac_client_name   = format("%s-%s", local.name, var.application_rbac_client_class)
  application_rbac_client_image  = var.application_rbac_client_image == null ? null : filebase64(var.application_rbac_client_image)
  application_rbac_client_owners = var.application_rbac_client_owners == null ? tolist([data.azuread_client_config.current.object_id]) : var.application_rbac_client_owners
}

###############################################################################
# Resources
###############################################################################

resource "azuread_application" "rbac_client" {
  count                          = local.rbac_create ? 1 : 0
  owners                         = local.application_rbac_client_owners
  logo_image                     = local.application_rbac_client_image
  display_name                   = local.application_rbac_client_name
  identifier_uris                = var.application_rbac_client_identifier_urls
  sign_in_audience               = var.application_rbac_client_audience
  terms_of_service_url           = var.application_rbac_client_terms_url
  privacy_statement_url          = var.application_rbac_client_privacy_statement_url
  group_membership_claims        = var.application_rbac_client_group_claims
  prevent_duplicate_names        = var.application_rbac_client_prevent_duplicates
  fallback_public_client_enabled = true

  feature_tags {
    hide                  = var.application_rbac_client_feature_hide
    gallery               = var.application_rbac_client_feature_gallery
    enterprise            = var.application_rbac_client_feature_enterprise
    custom_single_sign_on = var.application_rbac_client_feature_custom_sso
  }

  public_client {
    redirect_uris = var.application_rbac_client_reply_urls
  }

  required_resource_access {
    resource_app_id = "00000002-0000-0000-c000-000000000000"

    resource_access {
      id   = "311a71cc-e848-46a1-bdf8-97ff7156d8e6"
      type = "Scope"
    }
  }

  required_resource_access {
    resource_app_id = data.azuread_application.rbac_server[0].application_id

    resource_access {
      id   = data.azuread_application.rbac_server[0].api[0].oauth2_permission_scopes[0].id
      type = "Scope"
    }
  }
}

###############################################################################
# Outputs
###############################################################################

output "application_rbac_client_owners" {
  value = local.application_rbac_client_owners
}

output "application_rbac_client_audience" {
  value = var.application_rbac_client_audience
}

output "application_rbac_client_terms_url" {
  value = var.application_rbac_client_terms_url
}

output "application_rbac_client_reply_urls" {
  value = var.application_rbac_client_reply_urls
}

output "application_rbac_client_group_claims" {
  value = var.application_rbac_client_group_claims
}

output "application_rbac_client_identifier_urls" {
  value = var.application_rbac_client_identifier_urls
}

output "application_rbac_client_prevent_duplicates" {
  value = var.application_rbac_client_prevent_duplicates
}

output "application_rbac_client_privacy_statement_url" {
  value = var.application_rbac_client_privacy_statement_url
}

###############################################################################

output "application_rbac_client_feature_hide" {
  value = var.application_rbac_client_feature_hide
}

output "application_rbac_client_feature_gallery" {
  value = var.application_rbac_client_feature_gallery
}

output "application_rbac_client_feature_custom_sso" {
  value = var.application_rbac_client_feature_custom_sso
}

output "application_rbac_client_feature_enterprise" {
  value = var.application_rbac_client_feature_enterprise
}

###############################################################################

output "application_rbac_client_id" {
  value = length(azuread_application.rbac_client) == 1 ? azuread_application.rbac_client[0].application_id : null
}

output "application_rbac_client_name" {
  value = length(azuread_application.rbac_client) == 1 ? azuread_application.rbac_client[0].display_name : null
}

output "application_rbac_client_image" {
  value = length(azuread_application.rbac_client) == 1 ? azuread_application.rbac_client[0].logo_url : null
}

output "application_rbac_client_disabled" {
  value = length(azuread_application.rbac_client) == 1 ? azuread_application.rbac_client[0].disabled_by_microsoft : null
}

output "application_rbac_client_publisher" {
  value = length(azuread_application.rbac_client) == 1 ? azuread_application.rbac_client[0].publisher_domain : null
}

output "application_rbac_client_object_id" {
  value = length(azuread_application.rbac_client) == 1 ? azuread_application.rbac_client[0].object_id : null
}

output "application_rbac_client_app_role_ids" {
  value = length(azuread_application.rbac_client) == 1 ? azuread_application.rbac_client[0].app_role_ids : null
}

output "application_rbac_client_oauth2_permissions" {
  value = length(azuread_application.rbac_client) == 1 ? azuread_application.rbac_client[0].oauth2_permission_scope_ids : null
}

###############################################################################
