###############################################################################
# Optional Variables
###############################################################################

variable "application_rbac_server_name" {
  type        = string
  default     = null
  description = "The full name of the application registration used as the server component for AKS RBAC authentication."
}

###############################################################################
# Data Sources
###############################################################################

data "azuread_application" "rbac_server" {
  count        = local.rbac_create ? 1 : 0
  display_name = var.application_rbac_server_name
}

###############################################################################
# Outputs
###############################################################################

output "application_rbac_server_id" {
  value = length(data.azuread_application.rbac_server) == 1 ? data.azuread_application.rbac_server[0].application_id : null
}

output "application_rbac_server_api" {
  value = length(data.azuread_application.rbac_server) == 1 ? data.azuread_application.rbac_server[0].api : null
}

output "application_rbac_server_web" {
  value = length(data.azuread_application.rbac_server) == 1 ? data.azuread_application.rbac_server[0].web : null
}

output "application_rbac_server_name" {
  value = length(data.azuread_application.rbac_server) == 1 ? data.azuread_application.rbac_server[0].display_name : null
}

output "application_rbac_server_tags" {
  value = length(data.azuread_application.rbac_server) == 1 ? data.azuread_application.rbac_server[0].tags : null
}

output "application_rbac_server_image" {
  value = length(data.azuread_application.rbac_server) == 1 ? data.azuread_application.rbac_server[0].logo_url : null
}

output "application_rbac_server_owners" {
  value = length(data.azuread_application.rbac_server) == 1 ? data.azuread_application.rbac_server[0].owners : null
}

output "application_rbac_server_app_roles" {
  value = length(data.azuread_application.rbac_server) == 1 ? data.azuread_application.rbac_server[0].app_roles : null
}

output "application_rbac_server_audience" {
  value = length(data.azuread_application.rbac_server) == 1 ? data.azuread_application.rbac_server[0].sign_in_audience : null
}

output "application_rbac_server_disabled" {
  value = length(data.azuread_application.rbac_server) == 1 ? data.azuread_application.rbac_server[0].disabled_by_microsoft : null
}

output "application_rbac_server_features" {
  value = length(data.azuread_application.rbac_server) == 1 ? data.azuread_application.rbac_server[0].feature_tags : null
}

output "application_rbac_server_terms_url" {
  value = length(data.azuread_application.rbac_server) == 1 ? data.azuread_application.rbac_server[0].terms_of_service_url : null
}

output "application_rbac_server_object_id" {
  value = length(data.azuread_application.rbac_server) == 1 ? data.azuread_application.rbac_server[0].object_id : null
}

output "application_rbac_server_publisher" {
  value = length(data.azuread_application.rbac_server) == 1 ? data.azuread_application.rbac_server[0].publisher_domain : null
}

output "application_rbac_server_support_url" {
  value = length(data.azuread_application.rbac_server) == 1 ? data.azuread_application.rbac_server[0].support_url : null
}

output "application_rbac_server_app_role_ids" {
  value = length(data.azuread_application.rbac_server) == 1 ? data.azuread_application.rbac_server[0].app_role_ids : null
}

output "application_rbac_server_group_claims" {
  value = length(data.azuread_application.rbac_server) == 1 ? data.azuread_application.rbac_server[0].group_membership_claims : null
}

output "application_rbac_server_marketing_url" {
  value = length(data.azuread_application.rbac_server) == 1 ? data.azuread_application.rbac_server[0].marketing_url : null
}

output "application_rbac_server_public_client" {
  value = length(data.azuread_application.rbac_server) == 1 ? data.azuread_application.rbac_server[0].public_client : null
}

output "application_rbac_server_identifier_urls" {
  value = length(data.azuread_application.rbac_server) == 1 ? data.azuread_application.rbac_server[0].identifier_uris : null
}

output "application_rbac_server_optional_claims" {
  value = length(data.azuread_application.rbac_server) == 1 ? data.azuread_application.rbac_server[0].optional_claims : null
}

output "application_rbac_server_device_only_auth" {
  value = length(data.azuread_application.rbac_server) == 1 ? data.azuread_application.rbac_server[0].device_only_auth_enabled : null
}

output "application_rbac_server_resource_access_list" {
  value = length(data.azuread_application.rbac_server) == 1 ? data.azuread_application.rbac_server[0].required_resource_access : null
}

output "application_rbac_server_privacy_statement_url" {
  value = length(data.azuread_application.rbac_server) == 1 ? data.azuread_application.rbac_server[0].privacy_statement_url : null
}

output "application_rbac_server_single_page_application" {
  value = length(data.azuread_application.rbac_server) == 1 ? data.azuread_application.rbac_server[0].single_page_application : null
}

output "application_rbac_server_oauth2_permission_scope_ids" {
  value = length(data.azuread_application.rbac_server) == 1 ? data.azuread_application.rbac_server[0].oauth2_permission_scope_ids : null
}

output "application_rbac_server_oauth2_post_response_required" {
  value = length(data.azuread_application.rbac_server) == 1 ? data.azuread_application.rbac_server[0].oauth2_post_response_required : null
}

output "application_rbac_server_fallback_public_client_enabled" {
  value = length(data.azuread_application.rbac_server) == 1 ? data.azuread_application.rbac_server[0].fallback_public_client_enabled : null
}

###############################################################################
