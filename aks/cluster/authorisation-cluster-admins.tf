###############################################################################
# Optional Variables
###############################################################################

variable "authorisation_cluster_admins_role_name" {
  type        = string
  default     = "Azure Kubernetes Service Cluster User Role"
  description = "The role name for Azure Kubernetes Service cluster users."
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_role_assignment" "cluster_admins" {
  for_each             = data.azuread_group.rbac_admin_groups
  scope                = azurerm_kubernetes_cluster.object.id
  principal_id         = each.value.object_id
  role_definition_name = var.authorisation_cluster_admins_role_name
}

###############################################################################
# Outputs
###############################################################################

output "authorisation_cluster_admins_role_name" {
  value = var.authorisation_cluster_admins_role_name
}

###############################################################################

output "authorisation_cluster_admins_ids" {
  value = values(azurerm_role_assignment.cluster_admins)[*].id
}

output "authorisation_cluster_admins_principal_types" {
  value = values(azurerm_role_assignment.cluster_admins)[*].principal_type
}

###############################################################################
