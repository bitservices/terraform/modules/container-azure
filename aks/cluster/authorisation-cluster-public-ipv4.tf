###############################################################################
# Optional Variables
###############################################################################

variable "authorisation_cluster_public_ipv4_role_name" {
  type        = string
  default     = "Network Contributor"
  description = "The built-in role name for the AKS service principal to be granted on the public IPv4 addresses."
}

variable "authorisation_cluster_public_ipv4_skip_aad_check" {
  type        = bool
  default     = true
  description = "Skip Azure Active Directory checks which may fail due to replication lag. Should only be true for newly provisioned service principals."
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_role_assignment" "cluster_public_ipv4" {
  count                            = var.identity_type == "SystemAssigned" ? length(azurerm_public_ip.ipv4) : 0
  scope                            = azurerm_public_ip.ipv4[count.index].id
  principal_id                     = azurerm_kubernetes_cluster.object.identity[0].principal_id
  role_definition_name             = var.authorisation_cluster_public_ipv4_role_name
  skip_service_principal_aad_check = var.authorisation_cluster_public_ipv4_skip_aad_check
}

###############################################################################
# Outputs
###############################################################################

output "authorisation_cluster_public_ipv4_role_name" {
  value = var.authorisation_cluster_public_ipv4_role_name
}

output "authorisation_cluster_public_ipv4_skip_aad_check" {
  value = var.authorisation_cluster_public_ipv4_skip_aad_check
}

###############################################################################

output "authorisation_cluster_public_ipv4_ids" {
  value = azurerm_role_assignment.cluster_public_ipv4[*].id
}

output "authorisation_cluster_public_ipv4_principal_types" {
  value = azurerm_role_assignment.cluster_public_ipv4[*].principal_type
}

###############################################################################
