###############################################################################
# Optional Variables
###############################################################################

variable "authorisation_cluster_subnet_role_name" {
  type        = string
  default     = "Network Contributor"
  description = "The built-in role name for the AKS service principal to be granted on the AKS subnet."
}

variable "authorisation_cluster_subnet_skip_aad_check" {
  type        = bool
  default     = true
  description = "Skip Azure Active Directory checks which may fail due to replication lag. Should only be true for newly provisioned service principals."
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_role_assignment" "cluster_subnet" {
  count                            = var.identity_type == "SystemAssigned" ? 1 : 0
  scope                            = var.node_pool_subnet_id
  principal_id                     = azurerm_kubernetes_cluster.object.identity[0].principal_id
  role_definition_name             = var.authorisation_cluster_subnet_role_name
  skip_service_principal_aad_check = var.authorisation_cluster_subnet_skip_aad_check
}

###############################################################################
# Outputs
###############################################################################

output "authorisation_cluster_subnet_role_name" {
  value = var.authorisation_cluster_subnet_role_name
}

output "authorisation_cluster_subnet_skip_aad_check" {
  value = var.authorisation_cluster_subnet_skip_aad_check
}

###############################################################################

output "authorisation_cluster_subnet_id" {
  value = length(azurerm_role_assignment.cluster_subnet) == 1 ? azurerm_role_assignment.cluster_subnet[0].id : null
}

output "authorisation_cluster_subnet_principal_type" {
  value = length(azurerm_role_assignment.cluster_subnet) == 1 ? azurerm_role_assignment.cluster_subnet[0].principal_type : null
}

###############################################################################
