###############################################################################
# Required Variables
###############################################################################

variable "key" {
  type        = string
  sensitive   = true
  description = "The SSH public key to use for authenticating with Linux virtual machines."
}

variable "vnet" {
  type        = string
  description = "The full VNet name this AKS cluster will be built within."
}

variable "class" {
  type        = string
  description = "Identifier for this AKS cluster within its VNET."
}

variable "group" {
  type        = string
  description = "The full name of the resource group that will contain this AKS cluster."
}

variable "owner" {
  type        = string
  description = "Owner of the resource."
}

variable "company" {
  type        = string
  description = "Company the resource belogs to."
}

variable "location" {
  type        = string
  description = "Datacentre location for this resource group."
}

variable "password" {
  type        = string
  sensitive   = true
  description = "The password to use for authenticating with Windows virtual machines."
}

###############################################################################

variable "node_pool_subnet_id" {
  type        = string
  description = "The ID of the subnet that the default node pool for this AKS cluster will be built within."
}

###############################################################################
# Optional Variables
###############################################################################

variable "zones" {
  type        = list(number)
  default     = []
  description = "Availability zones this AKS cluster will span."
}

variable "release" {
  type        = string
  default     = "1.24.6"
  description = "The Kubernetes version to use for this AKS cluster."
}

variable "username" {
  type        = string
  default     = "aks"
  description = "The virtual machine operating system administrator username."
}

variable "dns_prefix" {
  type        = string
  default     = null
  description = "DNS prefix specified when creating the managed cluster."
}

variable "group_aks" {
  type        = string
  default     = null
  description = "The full name of the resource group to create that will contain this AKS clusters nodes and resources. Must NOT exist."
}

###############################################################################

variable "autoscaler_balance" {
  type        = bool
  default     = true
  description = "Detect similar node groups and balance the number of nodes between them."
}

variable "autoscaler_expander" {
  type        = string
  default     = "random"
  description = "Expander to use. Possible values are: 'least-waste', 'priority', 'most-pods' or 'random'."
}

variable "autoscaler_scale_down_max" {
  type        = number
  default     = 2
  description = "Maximum number of empty nodes that can be deleted at the same time."
}

variable "autoscaler_skip_system_pods" {
  type        = bool
  default     = true
  description = "If 'true' cluster autoscaler will never delete nodes with pods from kube-system (except for DaemonSet or mirror pods)."
}

variable "autoscaler_skip_local_storage" {
  type        = bool
  default     = false
  description = "If 'true' cluster autoscaler will never delete nodes with pods with local storage, for example, EmptyDir or HostPath."
}

variable "autoscaler_scale_down_utilisation" {
  type        = number
  default     = 0.7
  description = "Node utilization level, defined as sum of requested resources divided by capacity, below which a node can be considered for scale down."
}

###############################################################################

variable "identity_ids" {
  type        = list(string)
  default     = null
  description = "A list of user assigned identity IDs. Must already exist and is ignored if identity_type is 'SystemAssigned'."
}

variable "identity_type" {
  type        = string
  default     = "SystemAssigned"
  description = "The type of identity used for this AKS cluster. Can be 'SystemAssigned', 'UserAssigned' or 'SystemAssigned, UserAssigned'."
}

###############################################################################

variable "key_vault_secrets_enabled" {
  type        = bool
  default     = false
  description = "Enable built in Key Vault secrets CSI driver for AKS."
}

variable "key_vault_secrets_rotation_enabled" {
  type        = bool
  default     = false
  description = "Should secret rotation be enabled for the Key Vault secrets CSI driver for AKS. Ignored if 'key_vault_secrets_enabled' is 'false'."
}

variable "key_vault_secrets_rotation_interval" {
  type        = number
  default     = 2
  description = "The interval to poll for secret rotation in minutes. Ignored if 'key_vault_secrets_rotation_enabled' is 'false'."
}

###############################################################################

variable "load_balancer_sku" {
  type        = string
  default     = "standard"
  description = "Specifies the SKU of the Load Balancer used for this Kubernetes Cluster. Possible values are Basic and Standard."
}

variable "load_balancer_outbound_ports" {
  type        = number
  default     = 1280
  description = "The amount of outbound connections to be permitted per node. This number is used to calculate how many public IPv4 addresses to create for AKS outbound communication. Only applies if 'network_egress' is 'loadBalancer'."
}

###############################################################################

variable "nat_gateway_timeout" {
  type        = number
  default     = 5
  description = "Desired outbound flow idle timeout in minutes for the cluster NAT gateway. Only applies if 'network_egress' is 'managedNATGateway'."
}

variable "nat_gateway_ipv4_count" {
  type        = number
  default     = 1
  description = "Count of desired managed outbound IPs for the cluster NAT gateway. Only applies if 'network_egress' is 'managedNATGateway'."
}

###############################################################################

variable "network" {
  type        = string
  default     = "azure"
  description = "Network plugin type to use for networking."
}

variable "network_egress" {
  type        = string
  default     = "userAssignedNATGateway"
  description = "The outbound (egress) routing method which should be used for this Kubernetes Cluster. Possible values are 'loadBalancer', 'managedNATGateway', 'userAssignedNATGateway' or 'userDefinedRouting'."

  validation {
    condition     = contains(["loadBalancer", "managedNATGateway", "userAssignedNATGateway", "userDefinedRouting"], var.network_egress)
    error_message = "The network egress type must be either 'loadBalancer', 'managedNATGateway', 'userAssignedNATGateway' or 'userDefinedRouting'."
  }
}

###############################################################################

variable "node_pool_name" {
  type        = string
  default     = "pool"
  description = "The name of the default node pool for this AKS cluster."
}

variable "node_pool_size" {
  type        = string
  default     = "Standard_B2ms"
  description = "The virtual machine size of the default node pool for this AKS cluster."
}

variable "node_pool_count" {
  type        = number
  default     = 1
  description = "The minimum/initial number of nodes that make up the default node pool per availability zone for this AKS cluster."
}

variable "node_pool_count_max" {
  type        = number
  default     = 1
  description = "The maximum number of nodes that make up the default node pool per availability zone for this AKS cluster."
}

variable "node_pool_pod_limit" {
  type        = number
  default     = 100
  description = "The maximum number of pods that are allowed to run on each node."
}

variable "node_pool_max_surge_count" {
  type        = number
  default     = null
  description = "The maximum number of nodes which will be added to the default node pool per availability zone during an upgrade. Takes precedence over 'node_pool_max_surge_count'."
}

variable "node_pool_max_surge_percent" {
  type        = number
  default     = 20
  description = "The percentage of nodes which will be added to the default node pool per availability zone during an upgrade. Ignored if 'node_pool_max_surge_count' is set."
}

###############################################################################

variable "rbac_enabled" {
  type        = bool
  default     = true
  description = "Is Role Based Access Control based on Azure AD enabled?"
}

variable "rbac_admin_groups" {
  type        = set(string)
  default     = []
  description = "A list of names of Azure Active Directory Groups which should have Admin Role on the Cluster."
}

###############################################################################
# Locals
###############################################################################

locals {
  name                               = format("%s-%s", var.vnet, var.class)
  zones                              = distinct(var.zones)
  scripts                            = format("%s/scripts", path.module)
  templates                          = format("%s/templates", path.module)
  group_aks                          = coalesce(var.group_aks, local.name)
  dns_prefix                         = coalesce(var.dns_prefix, local.name)
  identity_ids                       = var.identity_type == "SystemAssigned" ? null : var.identity_ids
  network_cni                        = var.network == "azure" ? "azure" : "calico"
  rbac_create                        = local.rbac_managed ? false : var.rbac_enabled
  rbac_managed                       = var.application_rbac_server_name == null ? true : false
  rbac_admin_groups                  = local.rbac_managed ? var.rbac_admin_groups : []
  nat_gateway_timeout                = var.network_egress == "managedNATGateway" ? var.nat_gateway_timeout : null
  node_pool_count_max                = max(var.node_pool_count_max, var.node_pool_count)
  rbac_admin_group_ids               = [for group in data.azuread_group.rbac_admin_groups : group.object_id]
  node_pool_autoscaling              = local.node_pool_count_max > var.node_pool_count ? true : false
  nat_gateway_ipv4_count             = var.network_egress == "managedNATGateway" ? var.nat_gateway_ipv4_count : null
  node_pool_zones_default            = length(local.zones) >= 1 ? slice(local.zones, 0, 1) : null
  node_pool_count_max_zones          = local.node_pool_count_max * max(1, length(var.zones))
  node_pool_max_surge_value          = tostring(coalesce(var.node_pool_max_surge_count, format("%s%%", var.node_pool_max_surge_percent)))
  node_pool_max_surge_zones          = local.node_pool_max_surge_possible * max(1, length(var.zones))
  load_balancer_outbound_ports       = var.network_egress == "loadBalancer" ? var.load_balancer_outbound_ports : null
  node_pool_max_surge_possible       = coalesce(var.node_pool_max_surge_count, ceil((var.node_pool_max_surge_percent * local.node_pool_count_max) / 100))
  node_pool_zones_default_name       = local.node_pool_zones_default == null ? var.node_pool_name : format("%s%s", var.node_pool_name, local.node_pool_zones_default[0])
  key_vault_secrets_rotation_enabled = var.key_vault_secrets_enabled ? var.key_vault_secrets_rotation_enabled : false
}

###############################################################################
# Data Sources
###############################################################################

data "azuread_group" "rbac_admin_groups" {
  for_each         = local.rbac_admin_groups
  display_name     = each.value
  security_enabled = true
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_kubernetes_cluster" "object" {
  name                              = local.name
  location                          = var.location
  dns_prefix                        = local.dns_prefix
  kubernetes_version                = var.release
  node_resource_group               = local.group_aks
  resource_group_name               = var.group
  role_based_access_control_enabled = var.rbac_enabled

  tags = {
    "Name"         = local.name
    "RBAC"         = var.rbac_enabled ? (local.rbac_managed ? "Managed" : "Enabled") : "Disabled"
    "VNet"         = var.vnet
    "Class"        = var.class
    "Group"        = var.group
    "Owner"        = var.owner
    "Zones"        = length(local.zones) == 0 ? "Disabled" : join(", ", local.zones)
    "Company"      = var.company
    "Version"      = var.release
    "Location"     = var.location
    "Subscription" = data.azurerm_subscription.current.display_name
  }

  auto_scaler_profile {
    expander                         = var.autoscaler_expander
    empty_bulk_delete_max            = var.autoscaler_scale_down_max
    balance_similar_node_groups      = var.autoscaler_balance
    skip_nodes_with_system_pods      = var.autoscaler_skip_system_pods
    skip_nodes_with_local_storage    = var.autoscaler_skip_local_storage
    scale_down_utilization_threshold = var.autoscaler_scale_down_utilisation
  }

  dynamic "azure_active_directory_role_based_access_control" {
    for_each = var.rbac_enabled ? tolist([var.rbac_enabled]) : []

    content {
      managed                = local.rbac_managed
      client_app_id          = local.rbac_managed ? null : azuread_service_principal.rbac_client[0].application_id
      server_app_id          = local.rbac_managed ? null : data.azuread_service_principal.rbac_server[0].application_id
      server_app_secret      = local.rbac_managed ? null : azuread_service_principal_password.rbac_server[0].value
      azure_rbac_enabled     = local.rbac_managed ? true : null
      admin_group_object_ids = local.rbac_managed ? local.rbac_admin_group_ids : null
    }
  }

  default_node_pool {
    name                 = local.node_pool_zones_default_name
    zones                = local.node_pool_zones_default
    vm_size              = var.node_pool_size
    max_pods             = var.node_pool_pod_limit
    min_count            = local.node_pool_autoscaling ? var.node_pool_count : null
    max_count            = local.node_pool_autoscaling ? local.node_pool_count_max : null
    node_count           = local.node_pool_autoscaling ? null : var.node_pool_count
    vnet_subnet_id       = var.node_pool_subnet_id
    enable_auto_scaling  = local.node_pool_autoscaling
    orchestrator_version = var.release

    node_labels = {
      "k8s.bitservices.io/pool"         = var.node_pool_name
      "k8s.bitservices.io/vnet"         = var.vnet
      "k8s.bitservices.io/zone"         = local.node_pool_zones_default == null ? "Disabled" : local.node_pool_zones_default[0]
      "k8s.bitservices.io/group"        = local.group_aks
      "k8s.bitservices.io/cluster"      = local.name
      "k8s.bitservices.io/location"     = var.location
      "k8s.bitservices.io/subscription" = data.azurerm_subscription.current.display_name
    }

    upgrade_settings {
      max_surge = local.node_pool_max_surge_value
    }
  }

  identity {
    type         = var.identity_type
    identity_ids = local.identity_ids
  }

  linux_profile {
    admin_username = var.username

    ssh_key {
      key_data = var.key
    }
  }

  network_profile {
    outbound_type     = var.network_egress
    network_plugin    = var.network
    network_policy    = local.network_cni
    load_balancer_sku = var.load_balancer_sku

    dynamic "load_balancer_profile" {
      for_each = var.network_egress == "loadBalancer" ? [local.load_balancer_outbound_ports] : []

      content {
        outbound_ip_address_ids  = azurerm_public_ip.ipv4.*.id
        outbound_ports_allocated = load_balancer_profile.value
      }
    }

    dynamic "nat_gateway_profile" {
      for_each = var.network_egress == "managedNATGateway" ? [{ "timeout" = local.nat_gateway_timeout, "ipv4_count" = local.nat_gateway_ipv4_count }] : []

      content {
        idle_timeout_in_minutes   = nat_gateway_profile.value["timeout"]
        managed_outbound_ip_count = nat_gateway_profile.value["ipv4_count"]
      }
    }
  }

  windows_profile {
    admin_username = var.username
    admin_password = var.password
  }
}

###############################################################################
# Outputs
###############################################################################

output "key" {
  sensitive = true
  value     = var.key
}

output "vnet" {
  value = var.vnet
}

output "class" {
  value = var.class
}

output "group" {
  value = var.group
}

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

output "location" {
  value = var.location
}

output "password" {
  sensitive = true
  value     = var.password
}

###############################################################################

output "node_pool_subnet_id" {
  value = var.node_pool_subnet_id
}

###############################################################################

output "zones" {
  value = local.zones
}

output "release" {
  value = var.release
}

output "username" {
  value = var.username
}

output "dns_prefix" {
  value = local.dns_prefix
}

###############################################################################

output "autoscaler_balance" {
  value = var.autoscaler_balance
}

output "autoscaler_expander" {
  value = var.autoscaler_expander
}

output "autoscaler_scale_down_max" {
  value = var.autoscaler_scale_down_max
}

output "autoscaler_skip_system_pods" {
  value = var.autoscaler_skip_system_pods
}

output "autoscaler_skip_local_storage" {
  value = var.autoscaler_skip_local_storage
}

output "autoscaler_scale_down_utilisation" {
  value = var.autoscaler_scale_down_utilisation
}

###############################################################################

output "identity_type" {
  value = var.identity_type
}

###############################################################################

output "key_vault_secrets_enabled" {
  value = var.key_vault_secrets_enabled
}

output "key_vault_secrets_rotation_enabled" {
  value = local.key_vault_secrets_rotation_enabled
}

output "key_vault_secrets_rotation_interval" {
  value = var.key_vault_secrets_rotation_interval
}

###############################################################################

output "load_balancer_sku" {
  value = var.load_balancer_sku
}

output "load_balancer_outbound_ports" {
  value = local.load_balancer_outbound_ports
}

###############################################################################

output "nat_gateway_timeout" {
  value = local.nat_gateway_timeout
}

output "nat_gateway_ipv4_count" {
  value = local.nat_gateway_ipv4_count
}

###############################################################################

output "network" {
  value = var.network
}

output "network_cni" {
  value = local.network_cni
}

output "network_egress" {
  value = var.network_egress
}

###############################################################################

output "node_pool_name" {
  value = var.node_pool_name
}

output "node_pool_size" {
  value = var.node_pool_size
}

output "node_pool_count" {
  value = var.node_pool_count
}

output "node_pool_count_max" {
  value = local.node_pool_count_max
}

output "node_pool_pod_limit" {
  value = var.node_pool_pod_limit
}

output "node_pool_autoscaling" {
  value = local.node_pool_autoscaling
}

output "node_pool_count_max_zones" {
  value = local.node_pool_count_max_zones
}

output "node_pool_max_surge_value" {
  value = local.node_pool_max_surge_value
}

output "node_pool_max_surge_zones" {
  value = local.node_pool_max_surge_zones
}

output "node_pool_max_surge_possible" {
  value = local.node_pool_max_surge_possible
}

###############################################################################

output "node_pool_zones_default" {
  value = local.node_pool_zones_default
}

output "node_pool_zones_default_name" {
  value = local.node_pool_zones_default_name
}

###############################################################################

output "rbac_create" {
  value = local.rbac_create
}

output "rbac_enabled" {
  value = var.rbac_enabled
}

output "rbac_managed" {
  value = local.rbac_managed
}

output "rbac_admin_groups" {
  value = local.rbac_admin_groups
}

output "rbac_admin_group_ids" {
  value = local.rbac_admin_group_ids
}

###############################################################################

output "scripts" {
  value = local.scripts
}

output "templates" {
  value = local.templates
}

###############################################################################

output "id" {
  value = azurerm_kubernetes_cluster.object.id
}

output "name" {
  value = azurerm_kubernetes_cluster.object.name
}

output "group_aks" {
  value = azurerm_kubernetes_cluster.object.node_resource_group
}

output "fqdn_portal" {
  value = azurerm_kubernetes_cluster.object.portal_fqdn
}

output "fqdn_public" {
  value = azurerm_kubernetes_cluster.object.fqdn
}

output "kube_config" {
  sensitive = true
  value     = azurerm_kubernetes_cluster.object.kube_config
}

output "oidc_issuer" {
  value = azurerm_kubernetes_cluster.object.oidc_issuer_url
}

output "fqdn_private" {
  value = azurerm_kubernetes_cluster.object.private_fqdn
}

output "kube_config_raw" {
  sensitive = true
  value     = azurerm_kubernetes_cluster.object.kube_config_raw
}

output "kube_admin_config" {
  sensitive = true
  value     = azurerm_kubernetes_cluster.object.kube_admin_config
}

output "identity_tenant_id" {
  value = azurerm_kubernetes_cluster.object.identity[0].tenant_id
}

output "kube_admin_config_raw" {
  sensitive = true
  value     = azurerm_kubernetes_cluster.object.kube_admin_config_raw
}

output "kubelet_identity_id" {
  value = azurerm_kubernetes_cluster.object.kubelet_identity[0].user_assigned_identity_id
}

output "kubelet_identity_client_id" {
  value = azurerm_kubernetes_cluster.object.kubelet_identity[0].client_id
}

output "kubelet_identity_object_id" {
  value = azurerm_kubernetes_cluster.object.kubelet_identity[0].object_id
}

output "identity_service_principal_id" {
  value = azurerm_kubernetes_cluster.object.identity[0].principal_id
}

###############################################################################
