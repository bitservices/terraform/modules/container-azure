###############################################################################
# Optional Variables
###############################################################################

variable "node_pool_extra" {
  default     = []
  description = "List of extra node pool definitions. Each definition will be created per availability zone."

  type = list(object({
    name              = string
    size              = string
    count             = number
    count_max         = number
    pod_limit         = number
    subnet_id         = string
    max_surge_count   = number # Improve later with defaults and optional.
    max_surge_percent = number # Improve later with defaults and optional.
  }))
}

###############################################################################
# Locals
###############################################################################

locals {
  node_pool_extra_zones              = length(local.zones) >= 1 ? setproduct(var.node_pool_extra, local.zones) : setproduct(var.node_pool_extra, tolist([null]))
  node_pool_extra_count_max_total    = sum(concat(tolist([0]), [for node_pool_extra in var.node_pool_extra : max(node_pool_extra.count, node_pool_extra.count_max)]))
  node_pool_extra_count_max_zones    = local.node_pool_extra_count_max_total * max(1, length(var.zones))
  node_pool_extra_max_surge_zones    = local.node_pool_extra_max_surge_possible * max(1, length(var.zones))
  node_pool_extra_max_surge_possible = sum(concat(tolist([0]), [for node_pool_extra in var.node_pool_extra : coalesce(node_pool_extra.max_surge_count, ceil((node_pool_extra.max_surge_percent * max(node_pool_extra.count, node_pool_extra.count_max)) / 100))]))
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_kubernetes_cluster_node_pool" "extra" {
  count                 = length(local.node_pool_extra_zones)
  name                  = local.node_pool_extra_zones[count.index][1] == null ? local.node_pool_extra_zones[count.index][0].name : format("%s%s", local.node_pool_extra_zones[count.index][0].name, local.node_pool_extra_zones[count.index][1])
  zones                 = local.node_pool_extra_zones[count.index][1] == null ? null : tolist([local.node_pool_extra_zones[count.index][1]])
  vm_size               = local.node_pool_extra_zones[count.index][0].size
  max_pods              = local.node_pool_extra_zones[count.index][0].pod_limit
  min_count             = local.node_pool_extra_zones[count.index][0].count_max > local.node_pool_extra_zones[count.index][0].count ? local.node_pool_extra_zones[count.index][0].count : null
  max_count             = local.node_pool_extra_zones[count.index][0].count_max > local.node_pool_extra_zones[count.index][0].count ? local.node_pool_extra_zones[count.index][0].count_max : null
  node_count            = local.node_pool_extra_zones[count.index][0].count_max > local.node_pool_extra_zones[count.index][0].count ? null : local.node_pool_extra_zones[count.index][0].count
  vnet_subnet_id        = local.node_pool_extra_zones[count.index][0].subnet_id
  enable_auto_scaling   = local.node_pool_extra_zones[count.index][0].count_max > local.node_pool_extra_zones[count.index][0].count
  orchestrator_version  = var.release
  kubernetes_cluster_id = azurerm_kubernetes_cluster.object.id

  node_labels = {
    "k8s.bitservices.io/pool"         = local.node_pool_extra_zones[count.index][0].name
    "k8s.bitservices.io/vnet"         = var.vnet
    "k8s.bitservices.io/zone"         = local.node_pool_extra_zones[count.index][1] == null ? "Disabled" : local.node_pool_extra_zones[count.index][1]
    "k8s.bitservices.io/group"        = local.group_aks
    "k8s.bitservices.io/cluster"      = local.name
    "k8s.bitservices.io/location"     = var.location
    "k8s.bitservices.io/subscription" = data.azurerm_subscription.current.display_name
  }

  upgrade_settings {
    max_surge = tostring(coalesce(local.node_pool_extra_zones[count.index][0].max_surge_count, format("%s%%", local.node_pool_extra_zones[count.index][0].max_surge_percent)))
  }
}

###############################################################################

output "node_pool_extra" {
  value = var.node_pool_extra
}

###############################################################################

output "node_pool_extra_count_max_total" {
  value = local.node_pool_extra_count_max_total
}

output "node_pool_extra_count_max_zones" {
  value = local.node_pool_extra_count_max_zones
}

output "node_pool_extra_max_surge_zones" {
  value = local.node_pool_extra_max_surge_zones
}

output "node_pool_extra_max_surge_possible" {
  value = local.node_pool_extra_max_surge_possible
}

###############################################################################

output "node_pool_extra_ids" {
  value = azurerm_kubernetes_cluster_node_pool.extra.*.id
}

output "node_pool_extra_names" {
  value = azurerm_kubernetes_cluster_node_pool.extra.*.name
}

###############################################################################
