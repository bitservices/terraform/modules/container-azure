###############################################################################
# Locals
###############################################################################

locals {
  node_pool_zones_extra = length(local.zones) >= 2 ? slice(local.zones, 1, length(local.zones)) : []
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_kubernetes_cluster_node_pool" "zones_extra" {
  count                 = length(local.node_pool_zones_extra)
  name                  = format("%s%s", var.node_pool_name, local.node_pool_zones_extra[count.index])
  zones                 = tolist([local.node_pool_zones_extra[count.index]])
  vm_size               = var.node_pool_size
  max_pods              = var.node_pool_pod_limit
  min_count             = local.node_pool_autoscaling ? var.node_pool_count : null
  max_count             = local.node_pool_autoscaling ? var.node_pool_count_max : null
  node_count            = local.node_pool_autoscaling ? null : var.node_pool_count
  vnet_subnet_id        = var.node_pool_subnet_id
  enable_auto_scaling   = local.node_pool_autoscaling
  orchestrator_version  = var.release
  kubernetes_cluster_id = azurerm_kubernetes_cluster.object.id

  node_labels = {
    "k8s.bitservices.io/pool"         = var.node_pool_name
    "k8s.bitservices.io/vnet"         = var.vnet
    "k8s.bitservices.io/zone"         = local.node_pool_zones_extra[count.index]
    "k8s.bitservices.io/group"        = local.group_aks
    "k8s.bitservices.io/cluster"      = local.name
    "k8s.bitservices.io/location"     = var.location
    "k8s.bitservices.io/subscription" = data.azurerm_subscription.current.display_name
  }

  upgrade_settings {
    max_surge = local.node_pool_max_surge_value
  }
}

###############################################################################

output "node_pool_zones_extra" {
  value = local.node_pool_zones_extra
}

###############################################################################

output "node_pool_zones_extra_ids" {
  value = azurerm_kubernetes_cluster_node_pool.zones_extra.*.id
}

output "node_pool_zones_extra_names" {
  value = azurerm_kubernetes_cluster_node_pool.zones_extra.*.name
}

###############################################################################
