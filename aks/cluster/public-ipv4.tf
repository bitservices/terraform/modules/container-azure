###############################################################################
# Optional Variables
###############################################################################

variable "public_ipv4_version" {
  type        = string
  default     = "IPv4"
  description = "The version tag for IPv4. This should never be changed."
}

variable "public_ipv4_allocation" {
  type        = string
  default     = "Static"
  description = "Defines the allocation method for these IPv4 addresses. Possible values are Static or Dynamic."
}

variable "public_ipv4_idle_minutes" {
  type        = number
  default     = 30
  description = "Specifies the timeout for the TCP idle connection. The value can be set between 4 and 30 minutes."
}

###############################################################################
# Locals
###############################################################################

locals {
  public_ipv4_count      = var.network_egress == "loadBalancer" ? ceil(var.load_balancer_outbound_ports * (local.node_pool_count_max_zones + local.node_pool_max_surge_zones + local.node_pool_extra_count_max_zones + local.node_pool_extra_max_surge_zones) / 64000) : 0
  public_ipv4_allocation = lower(var.load_balancer_sku) == "standard" ? "Static" : var.public_ipv4_allocation
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_public_ip" "ipv4" {
  count                   = local.public_ipv4_count
  sku                     = var.load_balancer_sku
  name                    = format("%s-%s-%d", local.name, lower(var.public_ipv4_version), count.index + 1)
  location                = var.location
  ip_version              = var.public_ipv4_version
  allocation_method       = local.public_ipv4_allocation
  resource_group_name     = var.group
  idle_timeout_in_minutes = var.public_ipv4_idle_minutes

  tags = {
    "SKU"          = var.load_balancer_sku
    "Name"         = format("%s-%s-%d", local.name, lower(var.public_ipv4_version), count.index + 1)
    "VNet"         = var.vnet
    "Group"        = var.group
    "Index"        = count.index
    "Owner"        = var.owner
    "Cluster"      = local.name
    "Company"      = var.company
    "Version"      = var.public_ipv4_version
    "Location"     = var.location
    "Subscription" = data.azurerm_subscription.current.display_name
  }
}

###############################################################################
# Outputs
###############################################################################

output "public_ipv4_version" {
  value = var.public_ipv4_version
}

output "public_ipv4_allocation" {
  value = local.public_ipv4_allocation
}

output "public_ipv4_idle_minutes" {
  value = var.public_ipv4_idle_minutes
}

###############################################################################

output "public_ipv4_count" {
  value = local.public_ipv4_count
}

###############################################################################

output "public_ipv4_ids" {
  value = azurerm_public_ip.ipv4[*].id
}

output "public_ipv4_fqdns" {
  value = azurerm_public_ip.ipv4[*].fqdn
}

output "public_ipv4_addresses" {
  value = azurerm_public_ip.ipv4[*].ip_address
}

###############################################################################
