###############################################################################
# Optional Variables
###############################################################################

variable "service_principal_password_rbac_server_expires_days" {
  type        = number
  default     = 365
  description = "How many days the password will be valid for from time of creation."
}

###############################################################################
# Locals
###############################################################################

locals {
  service_principal_password_rbac_server_expires_days = var.service_principal_password_rbac_server_expires_days > 730 ? 730 : var.service_principal_password_rbac_server_expires_days
}

###############################################################################
# Resources
###############################################################################

resource "time_rotating" "rbac_server" {
  count         = local.rbac_create ? 1 : 0
  rotation_days = local.service_principal_password_rbac_server_expires_days
}

###############################################################################

resource "azuread_service_principal_password" "rbac_server" {
  count                = local.rbac_create ? 1 : 0
  service_principal_id = data.azuread_service_principal.rbac_server[0].object_id

  rotate_when_changed = {
    "rotation" = time_rotating.rbac_server[0].id
  }
}

###############################################################################
# Outputs
###############################################################################

output "service_principal_password_rbac_server_id" {
  value = length(azuread_service_principal_password.rbac_server) == 1 ? azuread_service_principal_password.rbac_server[0].key_id : null
}

output "service_principal_password_rbac_server_name" {
  value = length(azuread_service_principal_password.rbac_server) == 1 ? azuread_service_principal_password.rbac_server[0].display_name : null
}

output "service_principal_password_rbac_server_value" {
  sensitive = true
  value     = length(azuread_service_principal_password.rbac_server) == 1 ? azuread_service_principal_password.rbac_server[0].value : null
}

output "service_principal_password_rbac_server_end_date" {
  value = length(azuread_service_principal_password.rbac_server) == 1 ? azuread_service_principal_password.rbac_server[0].end_date : null
}

output "service_principal_password_rbac_server_start_date" {
  value = length(azuread_service_principal_password.rbac_server) == 1 ? azuread_service_principal_password.rbac_server[0].start_date : null
}

###############################################################################
