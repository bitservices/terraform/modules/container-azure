###############################################################################
# Optional Variables
###############################################################################

variable "service_principal_rbac_client_notes" {
  type        = string
  default     = null
  description = "A free text field to capture information about the service principal, typically used for operational purposes."
}

variable "service_principal_rbac_client_owners" {
  type        = list(string)
  default     = null
  description = "A list of object IDs that will be set as owners for this service principal. If unspecified the linked application owners are propogated."
}

variable "service_principal_rbac_client_enabled" {
  type        = bool
  default     = true
  description = "Whether or not the service principal account is enabled."
}

variable "service_principal_rbac_client_existing" {
  type        = bool
  default     = false
  description = "When 'true', any existing service principal linked to the same application will be automatically imported. Not recommended."
}

variable "service_principal_rbac_client_sso_mode" {
  type        = string
  default     = null
  description = "The single sign-on mode configured for this service principal. Azure AD uses the preferred single sign-on mode to launch the service principal from Microsoft 365 or the Azure AD My Apps. If specified, supported values are 'oidc', 'password', 'saml' or 'notSupported'."
}

variable "service_principal_rbac_client_login_url" {
  type        = string
  default     = null
  description = "The URL where the service provider redirects the user to Azure AD to authenticate. Azure AD uses the URL to launch the application from Microsoft 365 or the Azure AD My Apps."
}

variable "service_principal_rbac_client_description" {
  type        = string
  default     = "Managed by Terraform"
  description = "A description of the service principal provided for internal end-users."
}

variable "service_principal_rbac_client_alternative_names" {
  type        = set(string)
  default     = null
  description = "A set of alternative names for the service principal account."
}

variable "service_principal_rbac_client_notification_addresses" {
  type        = set(string)
  default     = null
  description = "A set of email addresses where Azure AD sends a notification when the active certificate is near the expiration date. This is only for the certificates used to sign the SAML token issued for Azure AD Gallery applications."
}

variable "service_principal_rbac_client_app_role_assignment_required" {
  type        = bool
  default     = false
  description = "Does this Service Principal require an AppRoleAssignment to a user or group before Azure AD will issue a user or access token to the application?"
}

###############################################################################

variable "service_principal_rbac_client_feature_hide" {
  type        = bool
  default     = false
  description = "Whether this service principal represents an application that is invisible to users in My Apps and Office 365 Launcher. If it is already enabled for the linked application, it will be propogated automatically."
}

variable "service_principal_rbac_client_feature_gallery" {
  type        = bool
  default     = false
  description = "Whether this service principal represents a gallery application. If it is already enabled for the linked application, it will be propogated automatically."
}

variable "service_principal_rbac_client_feature_custom_sso" {
  type        = bool
  default     = false
  description = "Whether this service principal represents a custom SAML application. If it is already enabled for the linked application, it will be propogated automatically."
}

variable "service_principal_rbac_client_feature_enterprise" {
  type        = bool
  default     = false
  description = "Whether this service principal represents an Enterprise Application. If it is already enabled for the linked application, it will be propogated automatically."
}

###############################################################################
# Locals
###############################################################################

locals {
  service_principal_rbac_client_owners             = coalesce(var.service_principal_rbac_client_owners, local.application_rbac_client_owners)
  service_principal_rbac_client_feature_hide       = var.application_rbac_client_feature_hide || var.service_principal_rbac_client_feature_hide
  service_principal_rbac_client_feature_gallery    = var.application_rbac_client_feature_gallery || var.service_principal_rbac_client_feature_gallery
  service_principal_rbac_client_feature_custom_sso = var.application_rbac_client_feature_custom_sso || var.service_principal_rbac_client_feature_custom_sso
  service_principal_rbac_client_feature_enterprise = var.application_rbac_client_feature_enterprise || var.service_principal_rbac_client_feature_enterprise
}

###############################################################################
# Resources
###############################################################################

resource "azuread_service_principal" "rbac_client" {
  count                         = local.rbac_create ? 1 : 0
  notes                         = var.service_principal_rbac_client_notes
  owners                        = local.service_principal_rbac_client_owners
  login_url                     = var.service_principal_rbac_client_login_url
  description                   = var.service_principal_rbac_client_description
  use_existing                  = var.service_principal_rbac_client_existing
  application_id                = azuread_application.rbac_client[0].application_id
  account_enabled               = var.service_principal_rbac_client_enabled
  alternative_names             = var.service_principal_rbac_client_alternative_names
  app_role_assignment_required  = var.service_principal_rbac_client_app_role_assignment_required
  notification_email_addresses  = var.service_principal_rbac_client_notification_addresses
  preferred_single_sign_on_mode = var.service_principal_rbac_client_sso_mode

  feature_tags {
    hide                  = local.service_principal_rbac_client_feature_hide
    gallery               = local.service_principal_rbac_client_feature_gallery
    enterprise            = local.service_principal_rbac_client_feature_enterprise
    custom_single_sign_on = local.service_principal_rbac_client_feature_custom_sso
  }
}

###############################################################################
# Outputs
###############################################################################

output "service_principal_rbac_client_id" {
  value = length(azuread_service_principal.rbac_client) == 1 ? azuread_service_principal.rbac_client[0].application_id : null
}

output "service_principal_rbac_client_name" {
  value = length(azuread_service_principal.rbac_client) == 1 ? azuread_service_principal.rbac_client[0].display_name : null
}

output "service_principal_rbac_client_type" {
  value = length(azuread_service_principal.rbac_client) == 1 ? azuread_service_principal.rbac_client[0].type : null
}

output "service_principal_rbac_client_audience" {
  value = length(azuread_service_principal.rbac_client) == 1 ? azuread_service_principal.rbac_client[0].sign_in_audience : null
}

output "service_principal_rbac_client_app_roles" {
  value = length(azuread_service_principal.rbac_client) == 1 ? azuread_service_principal.rbac_client[0].app_roles : null
}

output "service_principal_rbac_client_object_id" {
  value = length(azuread_service_principal.rbac_client) == 1 ? azuread_service_principal.rbac_client[0].object_id : null
}

output "service_principal_rbac_client_app_role_ids" {
  value = length(azuread_service_principal.rbac_client) == 1 ? azuread_service_principal.rbac_client[0].app_role_ids : null
}

output "service_principal_rbac_client_homepage_url" {
  value = length(azuread_service_principal.rbac_client) == 1 ? azuread_service_principal.rbac_client[0].homepage_url : null
}

output "service_principal_rbac_client_tenant_id" {
  value = length(azuread_service_principal.rbac_client) == 1 ? azuread_service_principal.rbac_client[0].application_tenant_id : null
}

output "service_principal_rbac_client_logout_url" {
  value = length(azuread_service_principal.rbac_client) == 1 ? azuread_service_principal.rbac_client[0].logout_url : null
}

output "service_principal_rbac_client_redirect_urls" {
  value = length(azuread_service_principal.rbac_client) == 1 ? azuread_service_principal.rbac_client[0].redirect_uris : null
}

output "service_principal_rbac_client_identifier_urls" {
  value = length(azuread_service_principal.rbac_client) == 1 ? azuread_service_principal.rbac_client[0].service_principal_names : null
}

output "service_principal_rbac_client_saml_metadata_url" {
  value = length(azuread_service_principal.rbac_client) == 1 ? azuread_service_principal.rbac_client[0].saml_metadata_url : null
}

output "service_principal_rbac_client_oauth2_permission_scopes" {
  value = length(azuread_service_principal.rbac_client) == 1 ? azuread_service_principal.rbac_client[0].oauth2_permission_scopes : null
}

output "service_principal_rbac_client_oauth2_permission_scope_ids" {
  value = length(azuread_service_principal.rbac_client) == 1 ? azuread_service_principal.rbac_client[0].oauth2_permission_scope_ids : null
}

output "service_principal_rbac_client_app_role_assignment_required" {
  value = length(azuread_service_principal.rbac_client) == 1 ? azuread_service_principal.rbac_client[0].app_role_assignment_required : null
}

###############################################################################
