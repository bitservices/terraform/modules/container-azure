###############################################################################
# Data Sources
###############################################################################

data "azuread_service_principal" "rbac_server" {
  count          = local.rbac_create ? 1 : 0
  application_id = data.azuread_application.rbac_server[0].application_id
}

###############################################################################
# Outputs
###############################################################################

output "service_principal_rbac_server_id" {
  value = length(data.azuread_service_principal.rbac_server) == 1 ? data.azuread_service_principal.rbac_server[0].application_id : null
}

output "service_principal_rbac_server_name" {
  value = length(data.azuread_service_principal.rbac_server) == 1 ? data.azuread_service_principal.rbac_server[0].display_name : null
}

output "service_principal_rbac_server_tags" {
  value = length(data.azuread_service_principal.rbac_server) == 1 ? data.azuread_service_principal.rbac_server[0].tags : null
}

output "service_principal_rbac_server_type" {
  value = length(data.azuread_service_principal.rbac_server) == 1 ? data.azuread_service_principal.rbac_server[0].type : null
}

output "service_principal_rbac_server_notes" {
  value = length(data.azuread_service_principal.rbac_server) == 1 ? data.azuread_service_principal.rbac_server[0].notes : null
}

output "service_principal_rbac_server_enabled" {
  value = length(data.azuread_service_principal.rbac_server) == 1 ? data.azuread_service_principal.rbac_server[0].account_enabled : null
}

output "service_principal_rbac_server_audience" {
  value = length(data.azuread_service_principal.rbac_server) == 1 ? data.azuread_service_principal.rbac_server[0].sign_in_audience : null
}

output "service_principal_rbac_server_saml_sso" {
  value = length(data.azuread_service_principal.rbac_server) == 1 ? data.azuread_service_principal.rbac_server[0].saml_single_sign_on : null
}

output "service_principal_rbac_server_sso_mode" {
  value = length(data.azuread_service_principal.rbac_server) == 1 ? data.azuread_service_principal.rbac_server[0].preferred_single_sign_on_mode : null
}

output "service_principal_rbac_server_app_roles" {
  value = length(data.azuread_service_principal.rbac_server) == 1 ? data.azuread_service_principal.rbac_server[0].app_roles : null
}

output "service_principal_rbac_server_login_url" {
  value = length(data.azuread_service_principal.rbac_server) == 1 ? data.azuread_service_principal.rbac_server[0].login_url : null
}

output "service_principal_rbac_server_tenant_id" {
  value = length(data.azuread_service_principal.rbac_server) == 1 ? data.azuread_service_principal.rbac_server[0].application_tenant_id : null
}

output "service_principal_rbac_server_object_id" {
  value = length(data.azuread_service_principal.rbac_server) == 1 ? data.azuread_service_principal.rbac_server[0].object_id : null
}

output "service_principal_rbac_server_logout_url" {
  value = length(data.azuread_service_principal.rbac_server) == 1 ? data.azuread_service_principal.rbac_server[0].logout_url : null
}

output "service_principal_rbac_server_description" {
  value = length(data.azuread_service_principal.rbac_server) == 1 ? data.azuread_service_principal.rbac_server[0].description : null
}

output "service_principal_rbac_server_app_role_ids" {
  value = length(data.azuread_service_principal.rbac_server) == 1 ? data.azuread_service_principal.rbac_server[0].app_role_ids : null
}

output "service_principal_rbac_server_homepage_url" {
  value = length(data.azuread_service_principal.rbac_server) == 1 ? data.azuread_service_principal.rbac_server[0].homepage_url : null
}

output "service_principal_rbac_server_redirect_urls" {
  value = length(data.azuread_service_principal.rbac_server) == 1 ? data.azuread_service_principal.rbac_server[0].redirect_uris : null
}

output "service_principal_rbac_server_identifier_urls" {
  value = length(data.azuread_service_principal.rbac_server) == 1 ? data.azuread_service_principal.rbac_server[0].service_principal_names : null
}

output "service_principal_rbac_server_alternative_names" {
  value = length(data.azuread_service_principal.rbac_server) == 1 ? data.azuread_service_principal.rbac_server[0].alternative_names : null
}

output "service_principal_rbac_server_saml_metadata_url" {
  value = length(data.azuread_service_principal.rbac_server) == 1 ? data.azuread_service_principal.rbac_server[0].saml_metadata_url : null
}

output "service_principal_rbac_server_notification_addresses" {
  value = length(data.azuread_service_principal.rbac_server) == 1 ? data.azuread_service_principal.rbac_server[0].notification_email_addresses : null
}

output "service_principal_rbac_server_oauth2_permission_scopes" {
  value = length(data.azuread_service_principal.rbac_server) == 1 ? data.azuread_service_principal.rbac_server[0].oauth2_permission_scopes : null
}

output "service_principal_rbac_server_oauth2_permission_scope_ids" {
  value = length(data.azuread_service_principal.rbac_server) == 1 ? data.azuread_service_principal.rbac_server[0].oauth2_permission_scope_ids : null
}

output "service_principal_rbac_server_app_role_assignment_required" {
  value = length(data.azuread_service_principal.rbac_server) == 1 ? data.azuread_service_principal.rbac_server[0].app_role_assignment_required : null
}

###############################################################################
