###############################################################################
# Terraform Settings
###############################################################################

terraform {
  required_providers {
    azuread = {
      source = "hashicorp/azuread"
    }

    azurerm = {
      source = "hashicorp/azurerm"
    }
  }
}

###############################################################################
